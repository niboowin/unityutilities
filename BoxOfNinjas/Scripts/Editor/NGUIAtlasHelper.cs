using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

public class NGUIAtlasHelper: AssetPostprocessor 
{
        static string atlas_path = "Textures/Atlases/";
        
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromPath)
    {
        foreach (string textAsset in importedAssets)
                {
                        if(textAsset.IndexOf(atlas_path) >= 0)
                        {
                                if(textAsset.IndexOf(".txt") == (textAsset.Length-4))
                                {
                                        string[] pathDir        = textAsset.Split(Path.AltDirectorySeparatorChar);
                                        string prefabName       = pathDir[pathDir.Length-2];
                                        pathDir                         = Regex.Split( textAsset, prefabName + ".txt");

                                        string prefabPath               = pathDir[0] + prefabName + ".prefab";
                                        string materialPath     = pathDir[0] + prefabName + ".mat";
                                        string texturePath              = pathDir[0] + prefabName + ".png";

                                        Material material = new Material (Shader.Find("Unlit/Transparent Colored"));
                                        AssetDatabase.CreateAsset(material, materialPath);
                                        AssetDatabase.Refresh();
                                        
                                        material = AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material)) as Material;
                                        material.mainTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(Texture2D));
                                        
                                        GameObject tempGameObject = new GameObject(prefabName);
                                        UIAtlas atlas                   = tempGameObject.AddComponent<UIAtlas>();
                                        atlas.spriteMaterial    = material;
                                        atlas.coordinates               = UIAtlas.Coordinates.TexCoords;

                                        TextAsset ta = AssetDatabase.LoadAssetAtPath(textAsset, typeof(TextAsset)) as TextAsset;
                                        NGUIJson.LoadSpriteData(atlas, ta);

                                        UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab(prefabPath);
                                        PrefabUtility.ReplacePrefab(tempGameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
                                        GameObject.DestroyImmediate(tempGameObject);

                                        AssetDatabase.SaveAssets();
                                        AssetDatabase.Refresh();
                                }
                        }
                }
    }



}